<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'test_wordpress');

/** Имя пользователя MySQL */
define('DB_USER', 'vagrant');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'vagrant');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'H~N6xm8;(mxX_Z[ EU{Qz&Qhr03*f)J-:$vPr%B~MRxpG[|*hN^i=tZ)|dN1K&+e');
define('SECURE_AUTH_KEY',  'W.]%Ri)Cm!Th/yR~n+vgv(Q2xAW4cPD%mgLw(T7w69S29IY|w?:f`Y%=pC]._JeA');
define('LOGGED_IN_KEY',    '(EO%dE(5N HT]rd7_G[i0LPrINl)NfK<_|a_>i N3Uju?h!VNewQ]3&G(0T_wYE&');
define('NONCE_KEY',        '_p{wWohjCPehVL-;+JYT%)0UdK=`wO8<-g)lLbPy)W!F(4=5.kaTDd.sJh4R5G?2');
define('AUTH_SALT',        'GN7CXrj<<+b,gshiuF36?m$s:uumOu(B`G`PBsi)u%E0G26|2-4=KOC|y%!q P*}');
define('SECURE_AUTH_SALT', ',W[~k=*]8VOy-B0Lg;:jjL};dn>Us:%r_=r|{` AFA ?JD(CmW=O^rhJ42GPcyZK');
define('LOGGED_IN_SALT',   'XU],{rU.MJEjz{H>k-Z4>9:A4KYhq;nzV;ZVX,=G,sRVLwJ|tkR,{W5U5PiX<0Vg');
define('NONCE_SALT',       'U.n^RM_5ecv6I/FtaLQ<T3{hBZx^Dt@aMf(lj_LcZTxD#c<:D[~2%#-VL Zubn[S');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
