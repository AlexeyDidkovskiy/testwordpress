<?php

class ClientClass
{
    private $formData = [
        'email',
        'result'
    ];

    function __construct()
    {
        do_action('admin_post_nopriv_ballot_form', [$this, 'formCallback']);
        do_action('admin_post_ballot_form', [$this, 'formCallback']);
    }

    public function formCallback()
    {
        var_dump($this->getFormData()); die();
    }

    private function getFormData()
    {
        return array_intersect_key($_POST, array_flip($this->formData));
    }
}