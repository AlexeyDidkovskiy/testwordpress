<?php
/*
 Plugin Name: Ballot Form
 */
?>

<?php

define('BALLOT_PLUGIN_PATH', plugin_dir_path(__FILE__));
define('BALLOT_PLUGIN_INC_PATH', BALLOT_PLUGIN_PATH . '/inc/');
define('BALLOT_PLUGIN_CLASSES_PATH', BALLOT_PLUGIN_INC_PATH . 'classes/');

include_once BALLOT_PLUGIN_CLASSES_PATH . 'ClientClass.php';

new ClientClass();