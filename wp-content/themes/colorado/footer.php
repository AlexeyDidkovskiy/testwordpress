<footer>
    <div class="container">
        <div class="footer-wrap">
            <div class="logo">
                <a href="<?php echo get_home_url(); ?>">
                    <img src="<?php echo bloginfo(template_url); ?>/images/logo-footer.png" alt="">
                </a>
            </div>
            <nav>
                <?php wp_nav_menu(array(
                    'theme_location' => 'menu',
                    'container' => 'false'
                ))?>
            </nav>
            <div class="social">
                <?php
                wp_nav_menu( array(
                    'theme_location' => 'social',
                    'menu_class'     => 'social-links-menu',
                    'container' => 'false'
                ) );
                ?>
            </div>
        </div>
        <div class="copy"><p>&#169; Copyright 2015 Lorem Ipsum</p></div>
    </div>
</footer>
<script>
</script>
</body>
</html>