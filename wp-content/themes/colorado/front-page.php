<?php
the_post();
//$fields1 = simple_fields_get_post_group_values(get_the_ID(), 'Videos', true, 2);
//var_dump($fields1);
//$image_info = wp_get_attachment_image_src('54');
//var_dump($image_info);
//die("hello1");
get_header();
?>

    <main>
        <?php
        $voteForm = simple_fields_get_post_group_values(get_the_ID(), 'Vote form', true, 2);
        if (is_array($voteForm) && count($voteForm)) {
            $voteForm = $voteForm[0];
        }
        ?>
        <?php $srcImageVoteFormBackground = wp_get_attachment_image_src($voteForm['Background'], "full")[0]; ?>
        <div class="map menu-anchor" id="map"
             style="background: url(<?php echo $srcImageVoteFormBackground; ?>)top center no-repeat;    background-size: cover;">
            <div class="container-full">
                <h2><?php echo $voteForm['Name Block']; ?></h2>
                <div class="map-box">
                    <?php if ($srcImageVoteFormImage = wp_get_attachment_image_src($voteForm['Image'], "full")[0]) { ?>
                        <img src="<?php echo $srcImageVoteFormImage; ?>" alt="">
                    <?php }; ?>
                </div>
                <div class="form-box">
                    <?php echo $voteForm['Question of vote']; ?>
                    <?php
                    $voteFormShortcode = $voteForm['Form'];
                    echo do_shortcode("$voteFormShortcode");
                    ?>
                </div>
            </div>
        </div>
        <div class="video-section menu-anchor" id="video">
            <div class="container">
                <h2>Video Section Header Text</h2>
                <?php
                $videoList = simple_fields_get_post_group_values(get_the_ID(), 'Videos', true, 2)
                ?>
                <div class="main-video">
                    <iframe src="<?php echo $videoList[0]['Link of video']; ?>" allowfullscreen></iframe>
                </div>
                <div class="gallery-video">
                    <?php foreach ($videoList as $videoItem) { ?>
                        <div class="item-video">
                            <a class="fancybox-media" href="<?php echo $videoItem['Link of video']; ?>">
                                <?php if ($srcImage = wp_get_attachment_image_src($videoItem['Image of video'], "full")[0]) { ?>
                                    <img src="<?php echo $srcImage; ?>" alt="">
                                <?php }; ?>
                            </a>
                            <p><?php $videoItem['Name of block'] ?></p>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="show-section menu-anchor" id="join">
            <div class="container">
                <?php
                $suportForm = simple_fields_get_post_group_values(get_the_ID(), 'Suport form', true, 2);
                if (is_array($suportForm) && count($suportForm)) {
                    $suportForm = $suportForm[0];
                }
                ?>
                <h2><?php echo $suportForm['Name Block']; ?></h2>
                <?php
                $formShortcode = $suportForm['Form'];
                echo do_shortcode("$formShortcode"); ?>
            </div>
        </div>
        <div class="about menu-anchor" id="about">
            <div class="container">
                <?php
                $aboutUs = simple_fields_get_post_group_values(get_the_ID(), 'About us', true, 2);
                if (is_array($aboutUs) && count($aboutUs)) {
                    $aboutUs = $aboutUs[0];
                }
                ?>
                <h2><?php echo $aboutUs['Name Block']; ?></h2>
                <div class="img-box">
                    <?php if ($srcImage = wp_get_attachment_image_src($aboutUs['Image'], "full")[0]) { ?>
                        <img src="<?php echo $srcImage; ?>" alt="">
                    <?php }; ?>
                </div>
                <div class="text-box">
                    <?php echo $aboutUs['Text of Block']; ?>
                </div>
            </div>
        </div>
    </main>
<?php
get_footer();