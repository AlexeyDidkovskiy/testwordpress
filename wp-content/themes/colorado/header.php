<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title><?php wp_title();?></title>
    <!-- google fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Dosis:400,600,700,800,500,300' rel='stylesheet' type='text/css'>

<?php wp_head()?>
</head>
<body>
<header id="header">
    <div class="container">
        <div class="logo">
            <a href="<?php echo get_home_url(); ?>">
        <?php
            if ( function_exists( 'the_custom_logo' ) ) {
                the_custom_logo();
            }
        ?>
            </a>
        </div>
        <nav>
            <?php wp_nav_menu(array(
                'theme_location' => 'menu',
                'container' => 'false'
            ))?>
        </nav>
        <div class="social">
            <?php
            wp_nav_menu( array(
                'theme_location' => 'social',
                'menu_class'     => 'social-links-menu',
                'container' => 'false'
            ) );
            ?>
        </div>
    </div>
</header>