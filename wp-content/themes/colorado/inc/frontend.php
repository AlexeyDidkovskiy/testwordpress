<?php

add_action('wp_enqueue_scripts', 'colorado_scripts');

function colorado_scripts()
{
    wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery-2.1.1.min.js');
    wp_enqueue_script('function', get_template_directory_uri() . '/js/function.js');
    wp_enqueue_script('fancybox', get_template_directory_uri() . '/js/jquery.fancybox.js');

    wp_enqueue_style('normalize', get_template_directory_uri() . '/css/normalize.css');
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
    wp_enqueue_style('fancybox', get_template_directory_uri() . '/css/jquery.fancybox.css');

    wp_enqueue_style('style', get_stylesheet_uri());
}

add_action( 'after_setup_theme', 'theme_prefix_setup' );

//init menu
register_nav_menus(
    array(
        'menu' => __( 'Header Menu', 'colorado' ),
        'social'  => __( 'Social Links Menu', 'colorado' )
    )
);

//options page
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page();
    the_repeater_field();

}

// logo
function theme_prefix_setup() {

    add_theme_support( 'custom-logo', array(
        'height'      => 100,
        'width'       => 400,
        'flex-width' => true,
    ) );
}