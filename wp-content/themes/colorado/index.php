<?php
    get_header();
?>

	<main>
		<div class="map" id="map">
			<div class="container-full">
				<h2>43 States States Sell Full Strength Beer or Wine in Grocery Stores -<br> Colorado is Not One of Them</h2>
				<div class="map-box">
					<img src="<?php echo bloginfo(template_url)?>/assets/images/Map.png" alt="">
				</div>
				<div class="form-box">
					<p>Vote Yes or No if You <br> Want Colorado to <br> Become the 44th</p>
					<form action="#" method="get">
						<input type="email" placeholder="Email Address">
						<input class="yellow " type="submit" value="yes">
						<input class="read" type="submit" value="no">
					</form>
				</div>
			</div>
		</div>
        <div class="video-section">
            <div class="container">
                <h2>Video Section Header Text</h2>
                    <div class="gallery-video">
                        <?php  if(have_posts()) : while (have_posts()) : the_post();?>

                            <div class="item-video">
                                <a class="fancybox-media" href="https://www.youtube.com/embed/aVS4W7GZSq0">
                                    <img src="<?php echo bloginfo(template_url)?>/assets/images/cap-video.jpg" alt="">
                                </a>
                                <p><?php the_content()?></p>
                            </div>

                        <?php endwhile;?>
                        <!-- post navigation -->
                        <?php else:?>
        <!--                no post found    -->
                        <?php endif;?>
                    </div>
            </div>
        </div>
		<div class="video-section" id="video">
			<div class="container">
				<h2>Video Section Header Text</h2>
				<div class="main-video">
					<iframe src="https://www.youtube.com/embed/aVS4W7GZSq0" allowfullscreen></iframe>
				</div>
				<div class="gallery-video">
					<div class="item-video">
						<a class="fancybox-media" href="https://www.youtube.com/embed/aVS4W7GZSq0">
							<img src="<?php echo bloginfo(template_url)?>/assets/images/cap-video.jpg" alt="">
						</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec.</p>
					</div>
					<div class="item-video">
						<a class="fancybox-media" href="https://www.youtube.com/embed/aVS4W7GZSq0">
							<img src="<?php echo bloginfo(template_url)?>/assets/images/cap-video.jpg" alt="">
						</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec.</p>
					</div>
					<div class="item-video">
						<a class="fancybox-media" href="https://www.youtube.com/embed/aVS4W7GZSq0">
							<img src="<?php echo bloginfo(template_url)?>/assets/images/cap-video.jpg" alt="">
						</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec.</p>
					</div>
					<div class="item-video">
						<a class="fancybox-media" href="https://www.youtube.com/embed/aVS4W7GZSq0">
							<img src="<?php echo bloginfo(template_url)?>/assets/images/cap-video.jpg" alt="">
						</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="show-section" id="join">
			<div class="container">
				<h2>Show Your Support</h2>
				<form action="#">
					<input type="email" placeholder="Email Address">
					<input type="text" placeholder="ZIP Code">
					<input class="animated wiggle" type="submit" value="Join the Movement">
				</form>
			</div>
		</div>
		<div class="about" id="about">
			<div class="container">
				<h2>About Us</h2>
				<div class="img-box">
					<img src="<?php echo bloginfo(template_url)?>/assets/images/lorem-img.jpg" alt="">
				</div>
				<div class="text-box">
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras nec risus a magna iaculis placerat. Nunc vitae erat laoreet, porta nisl et, consectetur ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas eleifend ac ante nec condimentum. Ut bibendum fermentum orci, eget dapibus ante pharetra et. Vestibulum vitae odio lorem. Vivamus arcu risus, venenatis eu metus ac, tempor tincidunt lectus. Etiam luctus dui viverra convallis accumsan.
						Phasellus sem leo, pharetra nec neque vel, mattis venenatis libero. Fusce porttitor ex eu est porta, nec iaculis dolor gravida. Praesent eleifend sodales nunc ac convallis. Donec id nisl ac orci mollis mattis. In est mauris, maximus elementum faucibus
					</p>
				</div>
			</div>
		</div>
	</main>
<?php
    get_footer();