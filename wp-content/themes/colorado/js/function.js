jQuery(document).ready(function() {
    jQuery(document).ready(function(){
        jQuery('a[href*=#]').bind("click", function(e){
	      var anchor = jQuery(this);
	      if(jQuery(window).width() > 803){
              jQuery('html, body').stop().animate({
	      	   scrollTop: jQuery(anchor.attr('href')).offset().top - 170
	      	}, 1000);
	      	e.preventDefault();
	      }else{
              jQuery('html, body').stop().animate({
	      	   scrollTop: jQuery(anchor.attr('href')).offset().top
	      	}, 1000);
	      }

	   });
	   return false;
	});


    jQuery('.fancybox-media').fancybox({
        	openEffect  : 'none',
			closeEffect : 'none',
            type: 'iframe',
			helpers : {
				media : {}
			}
        });


    jQuery("header nav>ul>li:first-of-type").addClass("active");
    jQuery("header nav>ul>li").on("click", function () {
        jQuery("header nav>ul>li").removeClass("active");
        jQuery(this).addClass("active");
    });


    jQuery('.email-field').on("input", function () {
        var validationEmail = jQuery(this).val();

        if (validationEmail.length > 0
            && (validationEmail.match(/^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/) || []).length !== 1) {
            jQuery('.btn-submit').addClass("not-active");
        } else {
            jQuery('.btn-submit').removeClass("not-active")
        }
    })

    jQuery('.wpcf7-form').on('submit', function(e) {

        var valueActiveSubmit = jQuery(document.activeElement)[0].value;

        jQuery("#vote", this).val(valueActiveSubmit);

        return true;
    });


});